#include "toneqoutport.h"

ToneQOutPort::ToneQOutPort(QString name, quint32 bufferSize, float samplerate, float freq, QObject *parent):
    QJack::QOutPort(name,bufferSize,parent)
{
    // Allocate an oscillator.
    cosc = new ComplexOscillator(samplerate,freq);
}

ToneQOutPort::~ToneQOutPort()
{
    // Clean up the complex oscillator because it is not a QObject.
    delete cosc;
}

void ToneQOutPort::setData(void *data, quint32 nFrames)
{
    float *fdata = (float *)data;

    // Jack wants nFrames of sample data. Give Jack what he wants.
    for(quint32 i=0; i<nFrames;i++)
    {
        // use the oscillator to get the required data.
        fdata[i] = cosc->getSample();
    }
}
