#include "rmsqinport.h"

RMSQInPort::RMSQInPort(QString name, quint32 bufferSize, float sampleRate, float timeConstant, QObject *parent):
    QJack::QInPort(name, bufferSize, parent)
{
    meanSquaredAmplitude = 0.;
    expfilt = new ExponentialFilter(sampleRate, timeConstant);
}

void RMSQInPort::setData(void *data, quint32 nframes)
{
    float *fdata = (float *)data;

    // We are to process nFrames samples of data.
    for(quint32 i=0; i<nframes;i++)
    {
        // Compute the square of the amplitude and filter it.
        meanSquaredAmplitude = expfilt->filter(fdata[i]*fdata[i]);
    }
}

float RMSQInPort::rmsAmplitude()
{
    return sqrt(meanSquaredAmplitude);
}
