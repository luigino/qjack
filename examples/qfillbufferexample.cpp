#include <iostream>
//#include <iomanip>
//#include <math.h>

#include <QCoreApplication>
#include <QObject>

#include "qjack/qjackclient.h"
#include "qjack/qfillbufferinport.h"

#include "peakaveragemeter.h"


using namespace std;
using namespace QJack;

//!
//! \brief main Connect up the Peak Average Meter
//! \param argc
//! \param argv
//! \return
//! A little demonstrator application for QFillBufferInPort.
//! It uses PeakAverageMeter connected to a QFillBufferInPort with
//! signals and slots to get a constant stream of data for the meter.
//! The key thing is that the buffers are not the same size as the JACK
//! buffer size - the difference is managed by QFillBufferInPort.
//!
int main(int argc, char** argv)
{
    QCoreApplication a(argc, argv);

    cout<<"QFillBufferInPort example starting."<<endl;

    // Allocate a Jack client and name it.
    QJackClient* client = new QJackClient("QFillBufferExample");

    // Open the client or bust.
    if (false == client->open()){
        exit(EXIT_FAILURE);
    }

    quint32 syssamplerate = client->sampleRate();
    quint32 buffersize = syssamplerate*2;

    // Create an QFillBufferInPort with a buffer with sufficient room for
    // two seconds worth of samples in the ringbuffer (inside superclass InPort).
    // Pass "client" as the QObject parent. Look it up.
    QFillBufferInPort qfbiport("input", buffersize, client);
    client->addPort(&qfbiport);

    cout<<"Activating client."<<endl;
    client->activate();

    // Create a PeakAverageMeter that will take 1-second snaps of sound.
    PeakAverageMeter pmeter(syssamplerate, client);

    // Use signal/slot to tell the pmeter when it has data to analyze.
    if( QObject::connect( &qfbiport, SIGNAL(filled()),
                              &pmeter, SLOT(gotSoundBuffer())
        )
    )
    {
        cout<<"Connected qfbiport signal filled() to pmeter slot gotSoundBuffer"<<endl;
    }
    else
    {
        cout<<"Failed to connect qfbiport signal filled() to pmeter slot gotSoundBuffer"<<endl;
    }

    // Use signal/slot to tell qfbiport that more data is needed.
    if( QObject::connect( &pmeter, SIGNAL(needSoundBuffer(jack_default_audio_sample_t *, quint32 )),
                          &qfbiport, SLOT(fill(jack_default_audio_sample_t *, quint32 ))
        )
    )
    {
        cout<<"Connected pmeter signal needSoundBuffer() to qfbiport slot fill"<<endl;
    }
    else
    {
        cout<<"Failed to connect pmeter signal needSoundBuffer() to qfbiport slot fill"<<endl;
    }

    cout << "Connecting newly created input port to system output port." << endl;
    QStringList outportnames = client->getPorts( NULL, NULL, (JackPortFlags)(JackPortIsOutput|JackPortIsPhysical) );

    if( outportnames.size() > 0 )    // Make sure there is at least one system output port.
    {
        if( 0 != client->connect(outportnames[0],&qfbiport) )   // Make the connection.
        {
            cout << "Failed to connect to system output port." << endl;
        }
        else
        {
            cout << "Connected to system output port." << endl;
        }
    }
    else
    {
        cout << "No output port available!" << endl;
    }

    cout << "Starting the PeakAverageMeter."<<endl<<endl;
    pmeter.start();

    cout<<"Entering Qt event loop. Hit interrupt to exit ..."<<endl<<endl;

    return a.exec();
}

