
#include <QtCore/QDebug>
#include "qjack/qjackclient.h"
#include "qjack/qsignalinport.h"
#include "qjack/qinport.h"
#include "qjack/qoutport.h"

using namespace QJack;

int main(int argc, char** argv)
{
    QJackClient* client = new QJackClient("dummy");

    // I open the client
    if (false == client->open()){
        exit(EXIT_FAILURE);
    }

    // It will send the jack data by a signal
    // connect yours QObject
    QSignalInPort *in_signalPort = new QSignalInPort("in_signal", client);

    // I create a port with a buffer with 2 * number of frames jack will pass me each time
    QInPort *in_port = new QInPort("in", client->nFrames() * 2, client);

    // I create a port with a buffer with 2 * number of frames jack will pass me each time
    QOutPort *out_port = new QOutPort("out", client->nFrames() * 2, client);

    // I add the ports to the client
    client->addPort(in_signalPort);
    client->addPort(in_port);
    client->addPort(out_port);

    // I activate the client
    client->activate();

    // the followiong case of use is terrible : it's only an example!
    float buff[client->nFrames()];

    while(1)
    {
        if (in_port->available() >= client->nFrames()){
            in_port->read(buff, client->nFrames());
            out_port->write(buff, client->nFrames());
        }
    }

    client->deactivate();
    client->close();

    delete(client);

    return (0);
}
