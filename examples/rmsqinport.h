#ifndef RMSQINPORT_H
#define RMSQINPORT_H

#include <math.h>

#include <qjack/qinport.h>
#include "exponentialfilter.h"

/*! \brief A QInPort that computes an RMS signal amplitude.
 * Calculates RMS power and averages using a simple RC-type filter.
 */
class RMSQInPort : public QJack::QInPort
{

public:
    /*! \brief Constructor
     * \param name The name of the resulting Jack port.
     * \param bufferSize How many frames Jack wants.
     * \param parent The QObject parent.
     */
    explicit RMSQInPort(QString name, quint32 bufferSize, float sampleRate, float timeConstant, QObject *parent = 0);
    
    /*! \brief Called when Jack requires data.
     * \param data A void pointer to the data buffer from Jack.
     * \param nFrames The number of frames that Jack requires.
     */
    void setData(void *data, quint32 nFrames);

    /*! \brief The RMS output amplitude for general consumption.
     */
    float meanSquaredAmplitude;

    /*! \brief Returns the square root of the mean squared amplitude, RMS.
     */
    float rmsAmplitude();

private:
    ExponentialFilter *expfilt;

};

#endif // RMSQINPORT_H
