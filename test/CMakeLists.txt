include_directories(${QT_INCLUDES} ${CMAKE_CURRENT_BINARY_DIR})

set(testqjackclient_SRCS testqjackclient.cpp)

qt4_automoc(${testqjackclient_SRCS})

add_executable(testqjackclient ${QT_QTCORE_LIBRARY} ${testqjackclient_SRCS})



